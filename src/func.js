const getSum = (str1, str2) => {
  if (typeof str1 !== "string" || typeof str2 !== "string") {
    return false;
  }
  str1 = isEmpty(str1);
  str2 = isEmpty(str2);
  let regexp = /\d/g;
  let strArrayLength = (str) => {
    if (str.match(regexp)) {
      return str.match(regexp).length;
    } else {
      return 0;
    }
  };

  if (
    strArrayLength(str1) !== str1.length &&
    strArrayLength(str2) !== str2.length
  ) {
    return false;
  }
  function isEmpty(str) {
    if (str.length === 0) {
      return "0";
    } else {
      return str;
    }
  }
  function numberParse(str) {
    return Number(str);
  }

  return `${numberParse(str1) + numberParse(str2)}`;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCount = 0,
    commentCount = 0;
  postCount = listOfPosts.filter((item) => item.author === authorName).length;
  for (const element of listOfPosts) {
    if (!element.comments) {
      continue;
    }
    commentCount += element.comments.filter(
      (person) => person.author === authorName
    ).length;
  }

  return `Post:${postCount},comments:${commentCount}`;
};

const tickets = (people) => {
  if (people[0] > 25) {
    return "NO";
  }
  let cashBox = 0,
    balance = 0;
  for (const person of people) {
    cashBox += 25;
    balance = person - 25;
    cashBox = cashBox - balance;
    if (cashBox < 0) {
      return "NO";
    }
  }
  return "YES";
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
